package com.shlomo.collatzconjectureapp
/**
 * Shlomo Bensimhon
 * 1837702
 *
 * 01/10/2021
 *
 * This class is used to calculate Collatz
 * conjecture.
 *
 *
 * No Partner, I Worked Alone.
 */

import android.content.Context
import android.text.method.ScrollingMovementMethod;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.security.AccessController.getContext

/**
 * The MainActivity class contains methods and variables that are
 * used to calculate the Collatz Conjecture.
 */

class MainActivity : AppCompatActivity() {
    private lateinit var button : Button
    private lateinit var highestNumber : TextView
    private lateinit var numberInput : TextView
    private lateinit var results : TextView
    var highestNum = 0;


    /**
     * This method is called when the layout of the App is created
     * and it initializes the global variables above, as well as
     * setting up the the scroll and preference features
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById<Button>(R.id.CalculateButton);
        highestNumber = findViewById<TextView>(R.id.HighestNumber);
        numberInput = findViewById<TextView>(R.id.inputNumber);
        results = findViewById<TextView>(R.id.resultsTextview);

        // this makes the textView holding the results scrollable
        results.movementMethod = ScrollingMovementMethod();

        val pref = getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = 0;
        highestNum = pref.getInt(getString(R.string.highest_number), defaultValue)
        highestNumber.text = getString(R.string.highest_number, highestNum.toString())
    }


    /**
     * This method is called when the calculate button is pressed.
     * It checks to see if the user input is valid and acts accordingly.
     */
    fun onSubmit(view : View) {
        results.text = getString(R.string.emptyString);
        var inputInt = numberInput.text.toString().toInt();

        if (inputInt == 0 || inputInt < 0) {
            Toast.makeText(applicationContext, getString(R.string.PleaseEnterPositive), Toast.LENGTH_SHORT).show();
        } else if(inputInt > highestNum) {
            highestNumber.text = getString(R.string.highest_number, inputInt.toString());
            highestNum = inputInt;
            verifyNumbers(inputInt);

        }else{
            verifyNumbers(inputInt);
        }
            onSave();
    }

        /**
         * This method is used to print the results from the computation
         * into a scrollable TextField named "results"
         */
        fun printResults(inputInt : Int){
            var currentInt = inputInt;
            while (currentInt != 1) {
                if ((currentInt % 2) == 0) {
                    currentInt = (currentInt / 2);
                } else {
                    currentInt = ((3 * currentInt) + 1);
                }
                results.append(currentInt.toString());
                results.append(", ");
            }
        }


        /**
         * This function is used to verify all the numbers from the
         * previously highest number, to the most recent highest number.
         * If a number is in violation of the Collatz Conjecture, the
         * method throws a message to the user and prints the results.
        */
        fun verifyNumbers(inputInt : Int){
            var tempHighestNum = highestNum
            var tempNum : Int;
            var list = mutableListOf<Int>();
            while(tempHighestNum < inputInt){
                if ((tempHighestNum % 2) == 0) {
                    tempNum = (tempHighestNum / 2);
                } else {
                    tempNum = ((3 * tempHighestNum) + 1);
                }
                if(list.contains(tempNum)){
                    Toast.makeText(applicationContext, getString(R.string.FoundViolation, inputInt.toString()), Toast.LENGTH_SHORT).show();
                }else{
                    list.add(tempNum);
                    tempHighestNum++;
                }
            }
            printResults(inputInt);
            }


         /**
          * This method is used to save the last highest number verified. It
          * uses the SharePreference class in order to do so. The saved variable
          * is stored even after the app is closed.
          */
        fun onSave(){
            val sharedPref = getPreferences(Context.MODE_PRIVATE) ?:
            return
            with (sharedPref.edit()) {
                putInt(getString(R.string.highest_number), highestNum)
                commit()
                }
            }
        }